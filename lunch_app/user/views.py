from rest_framework import generics, permissions, status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from user.authentication import TokenAuthentication
from user.permissions import IsRestaurantUser, IsSuperUser, IsCompanyUser
from user.serializers import UserSerializer, AuthTokenSerializer, RestaurantUserSerializer, CompanyUserSerializer


class CreateTokenView(ObtainAuthToken):
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class CreateRestaurantUserView(generics.CreateAPIView):
    serializer_class = RestaurantUserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (
        IsAuthenticated, IsRestaurantUser and IsAdminUser, IsSuperUser,
    )


class CreateCompanyUserView(generics.CreateAPIView):
    serializer_class = CompanyUserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (
        IsAuthenticated, IsCompanyUser and IsAdminUser, IsSuperUser,
    )


class ManageUserView(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        return self.request.user


class Logout(APIView):
    def get(self, request, format=None):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)
