from django.db.models import Count
from rest_framework import viewsets, generics
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from core.models import Restaurant, RestaurantMenu, VotedMenu
from core.utils import Today
from restaurant.serializers import RestaurantSerializer, RestaurantMenuSerializer, \
    RestaurantMenuSerializerWithRelatedInfo
from user.authentication import TokenAuthentication
from user.permissions import IsRestaurantUser


class RestaurantViewSet(viewsets.ModelViewSet):
    serializer_class = RestaurantSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (
        IsAuthenticated, IsRestaurantUser,
    )
    queryset = Restaurant.objects.all()


class RestaurantMenuViewSet(viewsets.ModelViewSet):
    serializer_class = RestaurantMenuSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsRestaurantUser,)
    queryset = RestaurantMenu.objects.all()

    def perform_create(self, serializer):
        restaurant_id = self.request.data.get('restaurant', None)
        queryset = self.queryset.filter(
            created_at__range=(Today.start(), Today.end()),
            restaurant=restaurant_id
        )

        if queryset.exists():
            raise ValidationError('A menu has been already submitted today')
        serializer.save()


class RestaurantMenuReadOnlyViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = RestaurantMenuSerializerWithRelatedInfo
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = RestaurantMenu.objects.all()

    def get_queryset(self):
        return self.queryset.filter(created_at__range=(Today.start(), Today.end()))


class CreateRestaurantWinnerView(generics.RetrieveAPIView):
    serializer_class = RestaurantMenuSerializerWithRelatedInfo
    queryset = RestaurantMenu.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        top_voted = VotedMenu.objects.all() \
            .values('restaurant_menu_id') \
            .annotate(total_voted=Count('user_id')) \
            .filter(created_at__range=(Today.start(), Today.end())) \
            .order_by('-total_voted') \
            .first()

        queryset = self.queryset.filter(pk=top_voted.get('restaurant_menu_id', None))
        obj = get_object_or_404(queryset)
        self.check_object_permissions(self.request, obj)
        return obj
