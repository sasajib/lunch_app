from rest_framework import serializers

from core.models import VotedMenu


class VotedMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = VotedMenu
        fields = ('id', 'restaurant_menu', 'user', 'has_voted', 'created_at')
        read_only_fields = ('id', 'created_at', 'has_voted')
