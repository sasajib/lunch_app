from django.urls import path

from user import views

app_name = 'user'

urlpatterns = [
    path('restaurant/create/', views.CreateRestaurantUserView.as_view(), name='create_restaurant_user'),
    path('company/create/', views.CreateRestaurantUserView.as_view(), name='create_restaurant_user'),
    path('token/', views.CreateTokenView.as_view(), name='token'),
    path('me/', views.ManageUserView.as_view(), name='me'),
    path('logout/', views.Logout.as_view()),
]
