from rest_framework import serializers

from core.models import Restaurant, RestaurantMenu


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'description', 'created_at', 'updated_at')
        read_only_fields = ('id', 'created_at',)


class RestaurantMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = RestaurantMenu
        fields = ('id', 'name', 'description', 'restaurant', 'created_at', 'updated_at')
        read_only_fields = ('id', 'created_at',)


class RestaurantMenuSerializerWithRelatedInfo(serializers.ModelSerializer):
    class Meta:
        model = RestaurantMenu
        fields = ('id', 'name', 'description', 'restaurant', 'created_at', 'updated_at')
        read_only_fields = ('id', 'created_at',)
        depth = 1
