### Task
```
A Company needs an internal service for its employees that can help them to make a decision
on selection of a lunch place. Each restaurant will be uploading menus using the system every
day over API and employees will vote for the menu before leaving for lunch.
Requirements for implementation:
● Develop Rest APIs for:
    ○ Authentication
    ○ Creating restaurant
    ○ Uploading menu for restaurant (There should be a menu for each day)
    ○ Creating employee
    ○ Getting current day menu
    ○ Voting for restaurant menu
    ○ Getting results for the current day. The winner restaurant should not be the
        winner for 3 consecutive working days (In that case, second highest vote holding
        restaurant should be the winner)
    ○ Logout
● Solution should be uploaded to version control
● Solution should be built using: Django and Python3, Django Rest Framework, SQL
    database of your choice (PostgreSQL, SQLite, MySQL, etc)
● Sufficient logging must be implemented
● PEP8 rules must be followed. Additional linters are welcomed (PyLint, etc)
● Project README.md must be created with launch instructions

Good to have:
- Write Test Case
- Dockerise the application
```

### Folder Structure
```
|- launch_app
  |- app        -(contains django releated settings and root route)
  |- core       -(contains dbmigrations, utils, admin settings)
  |- user       -(contains user related functionality)
  |- company    -(contains company related functionality)
  |- restaurant -(contains restaurant related functionality)
```

### Steps to run
```shell
#rename .env.bak file to .env
mv .env.bak .env 
docker-compose build
docker-compose up -d
docker-compose run --rm app sh -c "python manage.py migrate"
docker-compose run --rm app sh -c "python manage.py createsuperuser --email some@email.address"
docker-compose restart
```

### Admin endpoint
```
http://localhost:8000/admin
```

### Rest endpoints

* Create A User Token
```shell
#req
curl --location --request POST 'http://localhost:8000/api/user/token/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "some@email.address",
    "password": "SomePass123"
}'

#res
{
    "token": "a6b2ed138ecfd6e4c450de28402906273fdd44c3"
}
```

* Create A Restaurant User
```shell
#req
curl --location --request POST 'http://localhost:8000/api/user/restaurant/create/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "restaurant1@restaurant.com",
    "password": "SomePass123",
    "name": "restaurant1 user"
}'

#res
{
    "email": "restaurant1@restaurant.com",
    "name": "restaurant1 user"
}
```

* Create A Company Employee/User
```shell
curl --location --request POST 'http://localhost:8000/api/user/company/create/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "companyuser1@company.com",
    "password": "SomePass123",
    "name": "company user 1"
}'
```

* Create A Restaurant
```shell
curl --location --request POST 'http://localhost:8000/api/restaurant/restaurants/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "restaurant1",
    "description": {
        "some": "details"
    }
}'
```

* Create A Restaurant Menu
```shell
#req
curl --location --request POST 'http://localhost:8000/api/restaurant/menus/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "set menu 1",
    "description": {
        "some": "details"
    },
    "restaurant": 4
}'

#res
{
    "id": 3,
    "name": "set menu 1",
    "description": {
        "some": "details"
    },
    "restaurant": 4,
    "created_at": "2022-03-14T06:18:36.052493Z",
    "updated_at": "2022-03-14T06:18:36.052511Z"
}
```

* Get Today's Menu List
```shell
#req
curl --location --request GET 'http://localhost:8000/api/lunch/menu/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3'

#res
[
    {
        "id": 1,
        "name": "set menu 1",
        "description": {
            "description": "some description"
        },
        "created_at": "2022-03-14T07:22:51.002000Z",
        "updated_at": "2022-03-14T07:22:51.002000Z",
        "restaurant": {
            "id": 1,
            "name": "restaurant1",
            "description": {
                "description": "some description"
            },
            "created_at": "2022-03-13T07:22:00.415040Z",
            "updated_at": "2022-03-13T07:22:00.415059Z"
        }       
    },
    ...
]
```

* Vote a menu
```shell
#req
curl --location --request POST 'http://localhost:8000/api/lunch/vote/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3' \
--header 'Content-Type: application/json' \
--data-raw '{
    "restaurant_menu": 1,
    "user": 1
}'
#res
{
    "id": 5,
    "restaurant_menu": 1,
    "user": 1,
    "has_voted": true,
    "created_at": "2022-03-14T04:59:48.618855Z"
}
```
***Note**: for test purpose passing user id will work now, 
but the app will get from request session/token*

* Winner
```shell
#req
curl --location --request GET 'http://localhost:8000/api/lunch/winner/' \
--header 'Authorization: Bearer a6b2ed138ecfd6e4c450de28402906273fdd44c3'

#res
{
    "id": 1,
    "name": "set menu 1",
    "description": {
        "description": "some description"
    },
    "restaurant": {
        "id": 1,
        "name": "restaurant1",
        "description": {
            "description": "some description"
        },
        "created_at": "2022-03-13T07:22:00.415040Z",
        "updated_at": "2022-03-13T07:22:00.415059Z"
    },
    "created_at": "2022-03-14T07:22:51.002000Z",
    "updated_at": "2022-03-14T07:22:51.002000Z"
}
```

***did not implement feature- winner for 3 consecutive working days (In that case, second highest vote holding
        restaurant should be the winner)*


