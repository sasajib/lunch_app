from django.urls import path, include
from rest_framework.routers import DefaultRouter

from company import views
from restaurant import views as restaurant_view

router = DefaultRouter()
router.register('vote', views.VotedMenuViewSet)
router.register('menu', restaurant_view.RestaurantMenuReadOnlyViewSet)

app_name = 'company'

urlpatterns = [
    path('', include(router.urls)),
    path('winner/', restaurant_view.CreateRestaurantWinnerView.as_view(), name='winner')
]
