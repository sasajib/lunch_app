from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models


class LunchAppUserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_company_user(self, email, password=None, **extra_fields):
        user = self.create_user(email, password, **extra_fields)
        user.is_company_user = True
        user.save(using=self._db)
        return user

    def create_restaurant_user(self, email, password=None, **extra_fields):
        user = self.create_user(email, password, **extra_fields)
        user.is_restaurant_user = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email, password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class LunchAppUser(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_restaurant_user = models.BooleanField(default=False)
    is_company_user = models.BooleanField(default=False)
    objects = LunchAppUserManager()
    USERNAME_FIELD = 'email'


class Restaurant(models.Model):
    name = models.CharField(max_length=255)
    description = models.JSONField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class RestaurantMenu(models.Model):
    name = models.CharField(max_length=255)
    description = models.JSONField()
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']


class VotedMenu(models.Model):
    restaurant_menu = models.ForeignKey(RestaurantMenu, on_delete=models.CASCADE, )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, )
    has_voted = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']
        unique_together = ('restaurant_menu', 'user',)

    def __str__(self):
        return self.user.email + ':' + self.restaurant_menu.name
