from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext as _

from core import models


class UserAdmin(BaseUserAdmin):
    ordering = ['id']
    list_display = ['email', 'name']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal Info'), {'fields': ('name',)}),
        (
            _('Permissions'),
            {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_restaurant_user', 'is_company_user')}
        ),
        (_('Important dates'), {'fields': ('last_login',)})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }),
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'is_restaurant_user', 'is_company_user', "groups")


admin.site.register(models.LunchAppUser, UserAdmin)
admin.site.register(models.Restaurant)
admin.site.register(models.RestaurantMenu)
admin.site.register(models.VotedMenu)
