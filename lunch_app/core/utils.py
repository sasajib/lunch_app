import datetime

from django.utils import timezone


class Today:
    @staticmethod
    def start():
        return timezone.now().replace(hour=0, minute=0, second=0)

    @staticmethod
    def end():
        return timezone.now().replace(hour=23, minute=59, second=59)


class Days:
    @staticmethod
    def days_before_today(days):
        return timezone.now().replace(hour=0, minute=0, second=0) - datetime.timedelta(days=days)
