from rest_framework.permissions import BasePermission, IsAuthenticated, IsAdminUser


class IsRestaurantUser(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_restaurant_user)


class IsCompanyUser(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_company_user)


class IsSuperUser(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class RestaurantStaffPermission:
    permission_classes = (
        IsAuthenticated, IsRestaurantUser and IsAdminUser, IsSuperUser,
    )


class CompanyStaffPermission:
    permission_classes = (
        IsAuthenticated, IsCompanyUser and IsAdminUser, IsSuperUser,
    )


class RestaurantUserPermission:
    permission_classes = (
        IsAuthenticated, IsRestaurantUser, IsSuperUser,
    )


class CompanyUserPermission:
    permission_classes = (
        IsAuthenticated, IsCompanyUser, IsSuperUser,
    )
