from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

from company.serializers import VotedMenuSerializer
from core.models import VotedMenu, LunchAppUser
from core.utils import Today
from user.authentication import TokenAuthentication
from user.permissions import IsCompanyUser


class VotedMenuViewSet(viewsets.ModelViewSet):
    serializer_class = VotedMenuSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsCompanyUser,)
    queryset = VotedMenu.objects.all()

    def perform_create(self, serializer):
        # user = self.request.user
        # temporary for easier voting
        user_id = self.request.data.get('user', None)
        user = LunchAppUser.objects.all().filter(pk=user_id).first()

        queryset = self.queryset.filter(user=user, created_at__range=(Today.start(), Today.end()))
        if queryset.exists():
            raise ValidationError('Already voted')
        serializer.save(user=user)

    def perform_update(self, serializer):
        raise ValidationError('You can not cast vote again')

    def perform_destroy(self, instance):
        raise ValidationError('You cant not delete vote')
